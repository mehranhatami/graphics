#include <iostream>
#include "DrawingEngine.h"

int main(int argc, char* argv[]) {

    DrawingEngine drawingEngine;
    drawingEngine.run(argc, argv);

    return 0;
}