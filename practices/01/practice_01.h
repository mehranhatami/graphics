//
// Created by mehran on 1/14/18.
//

#ifndef UNIVERCITY_PRACTICES_PRACTICE_01_H
#define UNIVERCITY_PRACTICES_PRACTICE_01_H
#include <GL/glut.h>

class practice_01 {
public:
    void draw()
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(0,0,0);
        glBegin(GL_TRIANGLES);
        glVertex2i(0,0);
        glVertex2i(30,70);
        glVertex2i(-30,70);

        glVertex2i(40,0);
        glVertex2i(-30,-70);
        glVertex2i(30,-70);
        glEnd();
        glFlush();
    }
};



#endif //UNIVERCITY_PRACTICES_PRACTICE_01_H
