//
// Created by mehran on 1/14/18.
//

#ifndef UNIVERCITY_PRACTICES_BITMAP_STIPPLE_FONT_SAMPLE_H
#define UNIVERCITY_PRACTICES_BITMAP_STIPPLE_FONT_SAMPLE_H

#include <string>
using namespace std;

#include "../stdafx.h"
#include <GL/glut.h>



class BitmapStippleFontSample {

public:
    static void run(int practiceNumber, int argc, char* argv[])
    {
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
        glutInitWindowPosition(10,10);
        glutInitWindowSize(800,600);
        glutCreateWindow("Graphic with OpenGL");
        glClearColor(1.0,1.0,1.0,0.0);
        glMatrixMode(GL_PROJECTION);
        gluOrtho2D(-400,400,-300,300);
        if(practiceNumber == 1){
            glutDisplayFunc(draw1);
        } else if(practiceNumber == 2){
            glutDisplayFunc(draw2);
        } else if(practiceNumber == 3){
            glutDisplayFunc(draw3);
        }
        glutMainLoop();
    }

private:
    static void draw3()
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(0,0,0);
        GLubyte pattern[]={0x00,0x00,0x00,0x00,
                           0x00,0x00,0x00,0x00,
                           0x00,0x00,0x00,0x00,
                           0x00,0x00,0x00,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x0f,0xff,0xff,0xfe,
                           0x0f,0xff,0xff,0xfe,
                           0x0f,0xff,0xff,0xfe,
                           0x0f,0xff,0xff,0xfe,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x03,0xc0,0x00,
                           0x00,0x00,0x00,0x00};

        glEnable(GL_POLYGON_STIPPLE);
        glPolygonStipple(pattern);
        glBegin(GL_POLYGON);
        glVertex2i(0,0);
        glVertex2i(100,100);
        glVertex2i(0,200);
        glVertex2i(-100,100);
        glEnd();

        glFlush();
    }

    static void draw2()
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(0,0,0);
        glEnable(GL_LINE_STIPPLE);
        glLineStipple(1,0xFF00);
        glBegin(GL_LINES);
        glVertex2i(0,0);
        glVertex2i(400,0);
        glEnd();
        glLineStipple(4,0xFF08);
        glBegin(GL_LINES);
        glVertex2i(0,50);
        glVertex2i(400,50);
        glEnd();
        glFlush();
    }

    static void draw1()
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(0,0,0);
        glRasterPos2i(0,0);
        char text[]="Hello";
        for(int i=0;i<5;i++)
            glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,
                                text[i]);
        glFlush();
    }
};


#endif //UNIVERCITY_PRACTICES_BITMAP_STIPPLE_FONT_SAMPLE_H
