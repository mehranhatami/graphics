//
// Created by mehran on 1/14/18.
//

#ifndef UNIVERCITY_PRACTICES_PRACTICE_03_H
#define UNIVERCITY_PRACTICES_PRACTICE_03_H

#include <GL/glut.h>

class practice_03 {
public:
    void draw()
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(0,0,0);
        glBegin(GL_TRIANGLES);
        //glVertex2i(0,0);
        //glVertex2i(-50,-70);
        //glVertex2i(0,-140);

        // glVertex2i(80,0);
        //glVertex2i(80,-140);
        //glVertex2i(130,-70);

        glVertex2i(30,30);
        glVertex2i(0,0);
        glVertex2i(30,-30);
        glVertex2i(60,-30);
        glVertex2i(90,0);
        glVertex2i(60,30);
        //glVertex2i(20,-90);
        //glVertex2i(40,-80);

        glEnd();
        glFlush();
    }
};


#endif //UNIVERCITY_PRACTICES_PRACTICE_03_H
