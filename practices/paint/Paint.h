//
// Created by mehran on 1/14/18.
//

#ifndef UNIVERCITY_PRACTICES_PAINT_H
#define UNIVERCITY_PRACTICES_PAINT_H

#include <GL/glut.h>

class Paint {
public:
    static int x1,y1;

    static void motion(int x,int y)
    {
        x1=x-400;
        y1=300-y;
        glutPostRedisplay();
    }

    static int run(int argc, char * argv[])
    {
        glutInit(&argc,argv);
        glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
        glutInitWindowPosition(0,0);
        glutInitWindowSize(800,600);
        glutCreateWindow("Paint");
        glClearColor(1,1,1,0);
        glMatrixMode(GL_PROJECTION);
        gluOrtho2D(-400,400,-300,300);
        glutMotionFunc(motion);
        glutDisplayFunc(draw);
        glutMainLoop();
        return 0;
    }

    static void draw()
    {
        if(f)
        {
            glClear(GL_COLOR_BUFFER_BIT);
            f=false;
        }
        else
        {
            glColor3ub(0,0,0);
            glPointSize(4);
            glBegin(GL_POINTS);
            glVertex2i(x1,y1);
            glEnd();
        }
        glFlush();
    }

    static bool f;
};

#endif //UNIVERCITY_PRACTICES_PAINT_H
