//
// Created by mehran on 1/14/18.
//

#ifndef UNIVERCITY_PRACTICES_GRAPHICLANDSCAPE_H
#define UNIVERCITY_PRACTICES_GRAPHICLANDSCAPE_H

#include <GL/glut.h>

class GraphicLandscape {
public:
    static int run(int argc, char* argv[])
    {
        glutInit(&argc,argv);
        glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
        glutInitWindowPosition(0,0);
        glutInitWindowSize(1000,700);
        glutCreateWindow("Sample");
        glClearColor(1.0,1.0,1.0,0.0);
        glMatrixMode(GL_PROJECTION);
        gluOrtho2D(-500,500,-350,350);
        glutDisplayFunc(draw);
        glutMainLoop();
        return 0;
    }

private:
    static void draw()
    {
        glClear(GL_COLOR_BUFFER_BIT);
        //								grass
        glColor3ub(0,255,0);
        glBegin(GL_QUADS);
        glVertex2i(-500,-350);
        glVertex2i(500,-350);
        glVertex2i(500,-100);
        glVertex2i(-500,-100);
        glEnd();
        //								sky
        glColor3ub(120,200,255);
        glBegin(GL_QUADS);
        glVertex2i(-500,-100);
        glVertex2i(500,-100);
        glVertex2i(500,350);
        glVertex2i(-500,350);
        glEnd();
        //								mountains
        glColor3ub(230,180,0);
        glBegin(GL_TRIANGLES);
        glVertex2i(0,-100);
        glVertex2i(100,100);
        glVertex2i(200,-100);
        glVertex2i(200,-100);
        glVertex2i(500,-100);
        glVertex2i(350,200);
        glEnd();
        //								cloud
        glColor3ub(255,255,0);
        filledcircle(-350,250,50);
        glColor3ub(255,255,255);
        filledcircle(100,200,30);
        filledcircle(140,200,50);
        filledcircle(180,200,30);
        //								tree 1
        glColor3ub(0,150,0);
        glBegin(GL_TRIANGLES);
        glVertex2i(-400,-80);
        glVertex2i(-340,-80);
        glVertex2i(-370,-30);
        glVertex2i(-390,-50);
        glVertex2i(-350,-50);
        glVertex2i(-370,-10);
        glEnd();
        glColor3ub(150,70,0);
        glBegin(GL_QUADS);
        glVertex2i(-365,-80);
        glVertex2i(-375,-80);
        glVertex2i(-375,-100);
        glVertex2i(-365,-100);
        glEnd();
        //								tree 2
        glBegin(GL_QUADS);
        glVertex2i(-265,-60);
        glVertex2i(-255,-60);
        glVertex2i(-255,-100);
        glVertex2i(-265,-100);
        glEnd();
        glColor3ub(0,150,0);
        filledcircle(-275,-60,20);
        filledcircle(-245,-60,20);
        filledcircle(-260,-40,20);
        glFlush();
    }

    static void filledcircle(GLint xc,GLint yc, GLint r)
    {
        int r2=r*r;
        glBegin(GL_LINES);
        for(int x=0, y=r;x<=y;x++)
        {
            if(r2-x*x<y*y)
                y--;
            glVertex2i(xc+x,yc+y);
            glVertex2i(xc-x,yc+y);
            glVertex2i(xc+x,yc-y);
            glVertex2i(xc-x,yc-y);
            glVertex2i(xc+y,yc+x);
            glVertex2i(xc-y,yc+x);
            glVertex2i(xc+y,yc-x);
            glVertex2i(xc-y,yc-x);
        }
        glEnd();
    }
};


#endif //UNIVERCITY_PRACTICES_GRAPHICLANDSCAPE_H
