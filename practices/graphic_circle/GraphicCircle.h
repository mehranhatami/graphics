//
// Created by mehran on 1/14/18.
//

#ifndef UNIVERCITY_PRACTICES_GRAPHICCIRCLE_H
#define UNIVERCITY_PRACTICES_GRAPHICCIRCLE_H

#include <GL/glut.h>

class GraphicCircle {
public:
    static int run(int argc, char* argv[])
    {
        glutInit(&argc,argv);
        glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
        glutInitWindowPosition(0,0);
        glutInitWindowSize(800,600);
        glutCreateWindow("Sample");
        glClearColor(1.0,1.0,1.0,0.0);
        glMatrixMode(GL_PROJECTION);
        gluOrtho2D(-400,400,-300,300);
        glutDisplayFunc(draw);
        glutMainLoop();
        return 0;
    }

private:
    static void draw()
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(255,0,0);
        circle(-100,0,100);
        glColor3ub(0,255,0);
        circle(0,-175,100);
        glColor3ub(0,0,255);
        circle(100,0,100);
        glFlush();
    }

    static void circle(GLint xc,GLint yc, GLint r)
    {
        int r2=r*r;
        glBegin(GL_POINTS);
        for(int x=0, y=r;x<=y;x++)
        {
            if(r2-x*x<y*y)
                y--;
            glVertex2i(xc+x,yc+y);
            glVertex2i(xc-x,yc+y);
            glVertex2i(xc+x,yc-y);
            glVertex2i(xc-x,yc-y);
            glVertex2i(xc+y,yc+x);
            glVertex2i(xc-y,yc+x);
            glVertex2i(xc+y,yc-x);
            glVertex2i(xc-y,yc-x);
        }
        glEnd();
    }
};


#endif //UNIVERCITY_PRACTICES_GRAPHICCIRCLE_H
