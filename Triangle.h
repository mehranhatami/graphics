//
// Created by mehran on 1/13/18.
//

#ifndef UNIVERCITY_PRACTICES_TRIANGLE_H
#define UNIVERCITY_PRACTICES_TRIANGLE_H
#include <GL/glut.h>

class Triangle {
  int x1, y1,
    x2, y2,
    x3, y3;

public:
    Triangle(int _x1, int _y1, int _x2, int _y2, int _x3, int _y3){
        x1 = _x1;
        y1 = _y1;
        x2 = _x2;
        y2 = _y2;
        x3 = _x3;
        y3 = _y3;
    }

    void draw(){
        glBegin(GL_TRIANGLES);
        glVertex2i(x1, y1);
        glVertex2i(x2, y2);
        glVertex2i(x3, y3);
    }
};


#endif //UNIVERCITY_PRACTICES_TRIANGLE_H
