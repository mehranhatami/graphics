//
// Created by mehran on 1/13/18.
//

#ifndef UNIVERCITY_PRACTICES_DRAWINGENGINE_H
#define UNIVERCITY_PRACTICES_DRAWINGENGINE_H

#include <GL/glut.h>
#include "Triangle.h"
#include "practices/01/practice_01.h"
#include "practices/02/practice_02.h"
#include "libs/dda.h"
#include "libs/bresenham.h"
#include "practices/bitmap_stipple_font_sample/BitmapStippleFontSample.h"
#include "practices/paint/Paint.h"
#include "practices/graphic_landscape/GraphicLandscape.h"
#include "practices/graphic_fcircle/GraphicFCircle.h"
#include "practices/graphic_circle/GraphicCircle.h"

class DrawingEngine {
    const char *current_practice = "circle";
public:
    void run(int argc, char* argv[]){
        if(current_practice == "BitmapStippleFontSample"){
            BitmapStippleFontSample::run(3, argc, argv);
        } else if(current_practice == "Paint"){
            Paint::run(argc, argv);
        } else if(current_practice == "landscape"){
            GraphicLandscape::run(argc, argv);
        } else if(current_practice == "fcircle"){
            GraphicFCircle::run(argc, argv);
        } else if(current_practice == "circle"){
            GraphicCircle::run(argc, argv);
        }
        else {
            glutInit(&argc, argv);
            glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
            glutInitWindowPosition(100, 100);
            glutInitWindowSize(800, 600);
            glutCreateWindow("Mehran");
            gluOrtho2D(-400, 400, -300, 300);
            glClearColor(1, 1, 1, 1);
            glMatrixMode(GL_PROJECTION);
            glutDisplayFunc(draw);
            glutMainLoop();
        }
    }

    static void draw(){

//        dda _dda;
//        _dda.line(-180, 0, 90, 20);
//
//        bresenham _bresenham;
//        _bresenham.line(-180, -20, 90, 0);

//        practice_01 practice01;
//        practice01.draw();
//
//        practice_02 practice02;
//        practice02.draw();
//        glClear(GL_COLOR_BUFFER_BIT);
//        glColor3ub(0,0,0);
//
//        Triangle triangle (0, 0, 100, 0, 100, 100);
//        triangle.draw();
//
//        Triangle triangle2 (-300, 0, -100, 200, -100, -100);
//        triangle2.draw();
//
//        glBegin(GL_TRIANGLE_STRIP);
//
//        glVertex2i(0, 50);
//        glVertex2i(-50, 0);
//        glVertex2i(0,-50);
//        glVertex2i(150, -50);
//
//
//        glEnd();
//        glFlush();

        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(0,100,100);
        glBegin(GL_POLYGON);

        glVertex2i(0,0);
        glVertex2i(-60,-60);
        glVertex2i(-60,-144);
        glVertex2i(0,-204);
        glVertex2i(84,-204);
        glVertex2i(144,-144);
        glVertex2i(144,-60);
        glVertex2i(84,0);


        glEnd();
        glFlush();
    }
};


#endif //UNIVERCITY_PRACTICES_DRAWINGENGINE_H
