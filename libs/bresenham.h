//
// Created by mehran on 1/14/18.
//

#ifndef UNIVERCITY_PRACTICES_BRESENHAM_H
#define UNIVERCITY_PRACTICES_BRESENHAM_H

#include <GL/glut.h>

class bresenham {
public:
    void line(const int _x1, const int _y1, const int _x2, const int _y2){
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(0,0,0);
        glBegin(GL_POINTS);

        int x1 = _x1;
        int y1 = _y1;
        int x2 = _x2;
        int y2 = _y2;

        if(x1 > x2){
            x1 = _x2;
            y1 = _y2;
            x2 = _x1;
            y2 = _y1;
        }

        int dx = abs(x2 - x1);
        int dy = abs(y2 - y1);

        int steps = (abs(dx) > abs(dy)) ? abs(dx) : abs(dy);

        float two_dy = (2*dy);
        float two_dy_dx = (2*(dy-dx));

        int p = ((2*dy)-dx);


        float x = x1;
        float y = y1;

        glVertex2i(x, y);

        while (x < x2){
            x++;
            if(p<0){
                p+=two_dy;
            } else {
                y--;
                p+=two_dy_dx;
            }
            glVertex2i(x, y);
        }

        glEnd();
        glFlush();
    }
};


#endif //UNIVERCITY_PRACTICES_BRESENHAM_H
