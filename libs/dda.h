//
// Created by mehran on 1/14/18.
//

#ifndef UNIVERCITY_PRACTICES_DDA_H
#define UNIVERCITY_PRACTICES_DDA_H

#include <GL/glut.h>
#include <complex>

class dda {
public:
    void line(const int _x1, const int _y1, const int _x2, const int _y2){
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3ub(0,0,0);
        glBegin(GL_POINTS);

        int x1 = _x1;
        int y1 = _y1;
        int x2 = _x2;
        int y2 = _y2;

        if(x1 > x2){
            x1 = _x2;
            y1 = _y2;
            x2 = _x1;
            y2 = _y1;
        }

        float dx = x2 - x1;
        float dy = y2 - y1;

        int steps = (abs(dx) > abs(dy)) ? abs(dx) : abs(dy);

        float x_inc = (dx/(float)steps);
        float y_inc = (dy/(float)steps);

        float x = x1;
        float y = y1;

        glVertex2i(x, y);

        for(int count = 1; count<=steps;count++){
            x+=x_inc;
            y+=y_inc;
            glVertex2i((int)(x+0.5), (int)(y+0.5));
        }

        glEnd();
        glFlush();
    }
};


#endif //UNIVERCITY_PRACTICES_DDA_H
